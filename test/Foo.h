#ifndef FOO_H
#define FOO_H
#include <TObject.h>
#include <TString.h>
#include <bitset>

class Foo : public TObject {
public:
    Foo() : TObject(), fName("HUHU"), fA(1), fB(1) {
      for (int i=0;i<5;++i){
        fBits[i]=i%2;
      }
      fName = TString("A Foo");
      fA = -10;
      fB = -10;
  }
  
   Foo(double x, double y) : TObject(), fName("HAHA"), fA(x), fB(y), fBits( ) {
    for (int i=0;i<5;++i){
      fBits[i]=i%2;
    }
    fName = TString("A Foo");
   }

  void SetA(double);

public:
    TString fName;
    double fA;
    Double_t fB;
    std::bitset<5> fBits;
    ClassDef(Foo,1);
};

#endif
