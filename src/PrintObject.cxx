#include "ObjComparer.h"
#include "TFile.h"
#include "TSystem.h"
#include "TROOT.h"
#include <iostream>
#include <string>


// FIXME: share code between various tools
void printEntry(const char * fileName, const char * entry) {
  std::cout << "printing entry " << entry << " from file " << fileName << "\n";
  TFile *file = TFile::Open(fileName);
  if (!file) return;
  TObject *obj = (TObject*)file->Get(entry);

  if (!obj) {
    std::cerr << "File does not contain an object for entry " << entry << "\n";
    return;
  }
  
  ROOTObjComparer cmp;
  cmp.SetDebug(false);
  cmp.Print(obj);

  // for debugging purposes, can dump XML here via ROOT serialization
  // dump them to XML just to have a text comparison also
  TString outxml(fileName); outxml+=".xml";
  TFile *file1xml = TFile::Open(outxml.Data(), "RECREATE");
  if (file1xml){
    std::cout << "Writing XML for comparision\n";
    file1xml->WriteTObject(obj);
    file1xml->Close();
  }

  file->Close();
}

int main(int argc, char *argv[]) {
  // load the library holding information for the entry
  std::string ROOTVersion(gROOT->GetVersion());
  if ( ROOTVersion.find("6.") == std::string::npos ) {
    if (argc > 3){
      gSystem->Load(argv[3]);
    }
    else {
      std::cerr << "for ROOT5 we need to give the library containing the dictionary as 3rd argument\n";
    }
  }
  printEntry(argv[1], argv[2]);
  return 0;
}
