#include "ObjComparer.h"
#include "TFile.h"
#include "TSystem.h"
#include <iostream>

void printCDBEntry(const char * fileName) {
  std::cout << "printing CDB file " << fileName << "\n";
  TFile *file = TFile::Open(fileName);
  if (!file) return;
  TObject *obj = (TObject*)file->Get("AliCDBEntry");

  ROOTObjComparer cmp;
  cmp.Print(obj);

  // for debugging purposes, can dump XML here via ROOT serialization
  // dump them to XML just to have a text comparison also
  TString outxml(fileName); outxml+=".xml";
  TFile *file1xml = TFile::Open(outxml.Data(), "RECREATE");
  if (file1xml){
    std::cout << "Writing XML for comparision\n";
    file1xml->WriteTObject(obj);
    file1xml->Close();
  }

  file->Close();
}

void compareCDBEntry(const char * fileName1, const char *fileName2) {
  std::cout << "processing CDB file1 " << fileName1 << "\n";
  std::cout << "processing CDB file2 " << fileName2 << "\n";

  TFile *file1 = TFile::Open(fileName1);
  if (!file1) return;
  TObject *obj1 = (TObject*)file1->Get("AliCDBEntry");

  TFile *file2 = TFile::Open(fileName2);
  if (!file2) return;
  TObject *obj2 = (TObject*)file2->Get("AliCDBEntry");

  ROOTObjComparer cmp;
  cmp.SetVerbose(false);
  cmp.SetDebug(false);
  cmp.Diff(obj1, obj2);

  file1->Close();
  file2->Close();
}

int main(int argc, char *argv[]) {
  // load the library holding AliCDB information
  gSystem->Load("libCDB.so");


  // FIXME: we need better argument handling

  // check if printing mode
  if (argc > 2 && strcmp(argv[1],"-p")==0) {
    printCDBEntry(argv[2]);
    return 0;
  }

  // diff mode
  if (argc < 2) {
    std::cerr << "need to give 2 CDB ROOT files\n";
    return 1;
  }

  compareCDBEntry(argv[1], argv[2]);
  return 0;
}
