# ROOTObjComparer

A set of tools to inspect and compare objects serialized in ROOT files

## Examples

`PrintObject file.root Foo`  # prints out a textual representation of serialized entry Foo

`CDBCompare file1.root file2.root` # field-by-field diff of 2 AliCDB objects sitting in files file1 and file2


# Getting the source
    git clone https://gitlab.cern.ch/swenzel/ObjectCmp

# Installation

    source ROOT
    mkdir build
    cd build; cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/install
    make install

# Test

### make testlib known
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALLPATH/lib/

### create a test Foo object into `Foo.root`

    root WriteFoo.C #in the test directory
    
### print object

    PrintObject PATH_TO_FILE/Foo.root Foo #run the build/src directory
    
in case of ROOT5, we need to give the library (containing the Foo code in addition)

    PrintObject PATH_TO_FILE/Foo.root Foo libFoo.so #run the build/src directory


# Comparison of ALICE OCDB object

There is a special command to compare 2 ALICE OCDB objects (of type AliCDBEntry)

    CDBCompare OCDBFile1.root OCDBFile2.root #run the build/src directory